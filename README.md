# Output Allocation

*Output Allocation.py* is the Python code used by REF 2021 SP11 to allocate Outputs to reviewers. Details of the methods used can be found in the Working Methods document in the Documents project.

The same code was used, with different parameter settings, to allocate Impact Case Studies and Environment Statements.

We hope to be able to provide the real REF 2021 data in anonymised form to allow testing and experimentation, but that has not yet been agreed by the REF Team. If it is not agreed, we will consider generating comparable synthetic data.
