# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 18:49:18 2020

@author: Chris Taylor
"""
###############################################################################
''' 
Program to allocate outputs to reviewers, avoiding conflicts of interest,
matching expertise and balancing workload

Inputs:
    Spreadsheet with four worksheets
       Outputs: list with Output ID, Institution, Area #
       Expertise: reviewer x area table of reviewers' expertise levels
       Conflicts: reviewer x institution table of reviewer's conflicts 
       Strategy: list of parameters controlling the allocation algorithm
Outputs:
    Spreadsheet with six worksheets
       Allocation Results: list with Ouput ID, reviewers, and total expertise
       Reviewer Pairings: reviewer x reviewer table of paired and total loads
       Reviewer-Institute Pairings: reviewer x institute table of loads 
       Reviewer-Level: reviewer x expertise level table of loads
       Area_Expertise: area x expertise total table of loads
       Allocation Run Summary: summary of settings and results
Notes:
    The method involves assigning one reviewer to each output in each of a 
    series of passes, according to an expertise strategy, which gives the
    target expertise total for each output after each pass. The assignment
    algorithm is 'greedy', always assigning the least loaded qualifying
    reviewer. At each step, the load of a reviewer is a weighted sum of their
    individual load and the load they share with already assigned reviewers.
    The weight can be adjusted between 0 (only use pair load) and 1 (only use
    individual load). Multiple weights are tested, and the one that gives the 
    lowest sum of individual load variance and pair load variance is chosen. 
    The default is to perform a golden section search over a pre-defined range 
    of weight values, running the complete assignment process for each value of
    weight tested, but other strategies can be selected via Strategy parameters.
    The proportion of a reviewer's load they share with any other reviewer can 
    be capped, avoiding tight cliques. Individual reviewers' workloads can be 
    varied by providing a workload (0-1) row in the Expertise table; this 
    generates a workload weight for each reviewer, which is used to update 
    individual and pair loads during assignment. See the published SP11 Working 
    Methods document for more detail.
'''
###############################################################################

import tkinter.filedialog
import numpy as np
import math
import pandas as pd
import random as rand
import itertools as it
import statistics as stats
from datetime import datetime as dt

# Main reviewer assignment function, called once for each assignment pass
#########################################################################
def assign_revs(run, rnum, rev_col, targ_exp, r_wt):
    '''
    Assign reviewers for a given reviewer column and target expertise total
    Inputs:
        run : int
            The assignment run number, indicating where to store results
        rnum : int
            The reviewer number to be assigned on this pass
        rev_col : str, ['Rev1', 'Rev2', 'Rev3']
            Reviewer column to fill in outputs DataFrame
        targ_exp : int
            Target experience total after allocation
        r_wt : float
            Reviewer-Pair weight to use in load calculation
    Outputs:
        None
    Notes:
        Uses the following global variables/objects (in the interests of readability)
        Accesses: outputs, op_keys, revs_level_area, notrevs_inst, rev_pairs
        Modifies: outputs[rev_col, 'Expertise'], rev_pairs, inst_rev, rev_level
    '''
    # Use Total_0 column if this is the first pass, else use Total
    tot_row = 'Total_0' if rnum == 0 else 'Total'
    # Iterate over the outputs
    for op_num in op_keys:
        # Extract the set of already assigned reviewers
        assigned_revs = set(op_revs[run].loc[op_num, rev_cols])
        # Discard entries marked 'None'
        assigned_revs.discard('None')
        # Find highest feasible expertise level given target and current total
        top_lev = int(min(3, targ_exp - op_revs[run].loc[op_num, 'Expertise']))
        # Work down from highest feasible expertise level 
        for level in range(top_lev, 0, -1):
            # Find reviewers with appropriate expertise level for this output
            revs = revs_level_area[level][outputs.loc[op_num, 'Area #']]
            # Remove conflicted reviewers
            revs = set(revs) - set(xrevs_inst[outputs.loc[op_num, 'Institution']])
            # Remove reviewers already assigned to this output
            revs = revs - assigned_revs
            # If reviewer set is empty, try next level
            if revs == set(): continue
            # If first pass, create list of (candidate, candidate) pairs
            if rnum == 0: pairs = [(i, i) for i in revs]
            # Else create a list of (candidate, assigned) pairs
            else: pairs = list(it.product(revs, assigned_revs))
            # Extract list of reviewer loads for the candidate reviewer in each pair
            rev_loads = [rev_pairs[run].loc[tot_row, p[0]] for p in pairs]
            # Extract list of pair loads scaled to rev loads (same loads spread in 2D) 
            pair_loads = [(len(rev_keys) - 1) * rev_pairs[run].loc[p] for p in pairs]
            # Compute list of the weighted sum of reviewer and pair loads
            loads = [r_wt * r + (1 - r_wt) * p for r, p in zip(rev_loads, pair_loads)]
            # Find the location(s) for the pair(s) with the lowest load value
            min_pairs = [i for i, val in enumerate(loads) if val == min(loads)]
            # Choose pair at random (if more than 1) and place in to-inc list
            to_inc = [pairs[rand.sample(min_pairs, 1)[0]]] #[0]
            # Selected reviewer is first element of pair
            sel_rev = to_inc[0][0]
            # If first pass, jump out of loop with to_inc = [(selected, selected)]
            if rnum == 0: break
            # Otherwise expand to-inc with (selected, existing) reviewer pairs 
            to_inc = list(it.product({sel_rev}, assigned_revs))
            # Find the max load of all pairs set to be incremented  
            maxp = max([rev_pairs[run].loc[p] for p in to_inc])
            # If max load exceeds pair_frac try lower level, else jump out
            if maxp <= pair_frac * rev_pairs[run].loc['Total', sel_rev]: break
        # Record the selected reviewer for this reviewer column
        op_revs[run].loc[op_num, rev_col] = sel_rev
        # Increase expertise total for this output by level
        op_revs[run].loc[op_num, 'Expertise'] += level
        # Extend to_inc to include both orderings of all listed pairs
        to_inc = to_inc + [(b, a) for (a, b) in to_inc]
        # Increment cells in rev_pairs for all pairs in to_inc
        for p in to_inc:
            # Compute the total workload weight for pair p
            pair_loadwt = rev_loadwt[p[0]] + rev_loadwt[p[1]]
            # Increment pair cell by workload weight
            rev_pairs[run].loc[p] += pair_loadwt
            # Increment column total by workload weight
            rev_pairs[run].loc[tot_row, p[0]] += pair_loadwt
        # Increment relevant cells in inst-rev and rev-level           
        inst_rev[run].loc[outputs.loc[op_num, 'Institution'], sel_rev] += 1
        rev_level[run].loc[sel_rev, level] += 1
    return

#######################
# START OF MAIN PROGRAM
#######################

# Select an Excel file and read 4 worksheets into pandas DataFrames
###################################################################
# Define the root Tk window used for user dialogue
root = tkinter.Tk()
# Open the file browser widget in root Tk window
filename = tkinter.filedialog.askopenfilename(parent=root)
# Create a Pandas Excel filename object for the selected file
excel_file = pd.ExcelFile(filename)
# Read the four worksheets into Pandas DataFrames
outputs = pd.read_excel(excel_file, sheet_name='Outputs', index_col='O/P #')
expertise = pd.read_excel(excel_file, sheet_name='Expertise', index_col='Area #')
conflicts = pd.read_excel(excel_file, sheet_name='Conflicts', index_col='Institution')
strategy = pd.read_excel(excel_file, sheet_name='Strategy', index_col='Keywords')
# Kill the Tk windown
root.destroy()
# Randomise the order of outputs
outputs = outputs.reindex(np.random.permutation(outputs.index))

# Extract parameters from strategy DataFrame
############################################
# Number of revewers to assign to each output
num_revs = int(strategy.loc['Reviewers', 'Values'])
# Search strategy: default, auto or manual
search = strategy.loc['Search', 'Values']
# Number of reviewer load weights to use, if required
if search != 'default': num_wts = int(strategy.loc['Weights', 'Values'])
# Search strategy: default, auto or manual
search = strategy.loc['Search', 'Values']
# Cap for the proportion of a reviewer's load shared with any other reviewer
pair_frac = strategy.loc['Pair Fract', 'Values']
# Generate list of reviewer column headings of length num_revs
rev_cols = ['Rev' + str(i + 1) for i in range(int(num_revs))]
# Generate list of Target keys of length num_revs
target_keys = ['Target' + str(i + 1) for i in range(int(num_revs))]
# Extract the list of target values
targets = [int(strategy.loc[t, 'Values']) for t in target_keys]

# Set up weights for automated (Golden Section) or manual search
################################################################
# Compute Golden Ratio
gr = (1 + math.sqrt(5)) / 2
# Create dict for x1 - x4, initialised to point to first 4 test values
x = {1:0, 2:2, 3:3, 4:1}
# Define function to compute x2 in Golden Section search, given x1 and x4
comp_x2 = lambda x1, x4: wts[x4] - (wts[x4] - wts[x1]) / gr
# Define function to compute x3 in Golden Section search, given x1 and x4
comp_x3 = lambda x1, x4: wts[x1] + (wts[x4] - wts[x1]) / gr
# Test if search mode is set to default 
if search == 'default':
    # If so, create wts list and set bracket values
    wts = [0.7, 0.99, 0, 0, 0, 0, 0, 0, 0]
# Test if search mode is set to auto
elif search == 'auto':
    # If so, extract the user-supplied bracket values
    wts = [strategy.loc[w, 'Values'] for w in ['Weight1', 'Weight2']]
    # Pad the list to total length num_wts
    wts = wts + [0] * (num_wts - 2)
# Test if search mode is set to one of the automated modes
if search == 'default' or search == 'auto':
    # If so, compute weight at x2
    wts[x[2]] = comp_x2(x[1], x[4])
    # Compute weight at x3
    wts[x[3]] = comp_x3(x[1], x[4])
# Search mode set to manual
else:
    # Generate list of Weight keys of length num_wts
    wt_keys = ['Weight' + str(i + 1) for i in range(int(num_wts))]
    # Extract the user-supplied weights from the strategy DataFrame
    wts = [strategy.loc[w, 'Values'] for w in wt_keys]
# In all cases set num_wts to the length of the weights list
num_wts = len(wts)
 
# Extract reviewer, area, institution and output keys from input data
#####################################################################
# Get reviewer keys skipping Area # and Research Area headings
rev_keys = expertise.columns.tolist()[1:]
# Get research area keys
area_keys = expertise.index.tolist()
# Get institution keys
inst_keys = conflicts.index.tolist()
# Get output keys
op_keys = outputs.index.tolist()

# Extract reviewer workloads if included in Expertise worksheet 
###############################################################
# Check if WL key is present, indicating reviewer workloads are provided
if 'WL' in area_keys:
    # If so, remove from area keys
    area_keys.remove('WL')
    # Extract reviewer workloads from expertise DataFrame
    rev_wloads = [expertise.loc['WL', r] for r in rev_keys]
else:
    # Otherwise set all reviewer workloads to 1
    rev_wloads = [1] * len(rev_keys)
# Calculate the reviewer load weightings to use from reviewer workloads
rev_loadwt = dict(zip(rev_keys, [(2-w)/w for w in rev_wloads]))

# Create dicts allowing look-up of reviewers by area/level and intitution conflict 
##################################################################################
# Create empty dict indexed by expertise level
revs_level_area = {1: 0, 2: 0, 3: 0}
# Cycle through expertise levels
for lev in revs_level_area.keys():
    # For each research area, find the list reviewers with expertise level lev 
    r_lists = [[r for r in rev_keys if expertise.loc[a, r] == lev] for a in area_keys]
    # Create dict of reviewer lists indexed by research area for this expertise level 
    revs_level_area [lev] = dict(zip(area_keys, r_lists))
# Create a list for each institution of conflicted reviewers
xrev_lists = [[r for r in rev_keys if conflicts.loc[i, r] == 1] for i in inst_keys]
# Create a dict of lists allowing look-up of conflicted reviewers by institution
xrevs_inst = dict(zip(inst_keys, xrev_lists))

# Create DataFrames and lists for accumulating results
###################################################### 
# Create list (by rev weight) of reviewer assignment DataFrames with Rev[i] = None
op_revs = [pd.DataFrame(data='None', index=op_keys, columns=rev_cols) for i in wts]
# Add a zeroed Expertise column to each DataFrame
for i in range(num_wts): op_revs[i]['Expertise'] = 0
# Create keys for rev_pairs table by adding Total and Total_0 keys 
tab_keys = rev_keys + ['Total', 'Total_0']
# Create list (by rev weight) of zeroed DataFrames to hold reviewer and pair loads
rev_pairs = [pd.DataFrame(data=0, index=tab_keys, columns=rev_keys) for i in wts]
# Create list (by rev weight) of zeroed DataFrames to hold institution-reviewer loads
inst_rev = [pd.DataFrame(data=0, index=inst_keys, columns=rev_keys) for i in wts]
# Create list (by rev weight) of zeroed DataFrames to hold reviewer-level loads
rev_level = [pd.DataFrame(data=0, index=rev_keys, columns=(3, 2, 1)) for i in wts]
# Create zeroed list (by rev weight) for reviewer-load standard deviation
rsd = [0] * num_wts
# Create zeroed list (by rev weight) of pair-load standard deviation
psd = [0] * num_wts
# Create zeroed list (by rev weight) of total variance
tvar = [0] * num_wts

# Assign reviewers using the assignment strategy, using range of rev-pair weights  
#################################################################################
# Loop over weights
for k in range(num_wts):
    # Golden section search if automated mode selected and 4 evaluations complete
    if (search == 'default' or search == 'auto') and k >3:
        # Test if f(x2) < f(x3)
        if tvar[x[2]] < tvar[x[3]]:
            # If so x3 -> x4, x2 -> x3, x1 unchanged
            for i in range(4, 2, -1): x[i] = x[i - 1]
            # Compute new value for x2
            wts[k] = comp_x2(x[1], x[4])
            # Set x2 to point to new value
            x[2] = k
        else:
            # Otherwise x2 -> x1, x3 -> x2, x4 unchanged
            for i in range(1, 3): x[i] = x[i + 1]
            # Compute new value for x3
            wts[k] = comp_x3(x[1], x[4])
            # Set x3 to point to new value
            x[3] = k
    # Loop over number of reviewers to assign
    for r in range(num_revs):
        # Assign a reviewer for each output and store results in datastructures k
        assign_revs(k , r, rev_cols[r], targets[r], wts[k])
    # Compute SD of reviewer loads, scaled to be comensurate with SD of pair loads  
    rsd[k] = stats.stdev(rev_pairs[k].loc['Total', rev_keys]) / (num_revs - 1)  
    # Create list of reviewer pairs by list index, so > test can be used below
    p = list(it.product(range(len(rev_keys)), range(len(rev_keys))))
    # Calculate the SD of pair loads in lower triangle for weighting k
    psd[k] = stats.stdev([float(rev_pairs[k].iloc[i]) for i in p if i[0] > i[1]])
    # Compute total variance for weighting k
    tvar[k] = rsd[k]**2 + psd[k]**2
    
# Select best result and generate additional result tables
##########################################################
# Find the weight that gave the lowest total variance
sel_wt = np.argmin(tvar)
# Append the corresponding reviewer assignment DataFrame to the outputs df
outputs = pd.concat([outputs, op_revs[sel_wt]], axis=1)
# Select the pair data from the corresponding rev pairs DataFrame
rpairs_out = rev_pairs[sel_wt].loc[rev_keys, rev_keys]
# Cycle through rpairs_out to compute actual rather than relative loads
for (a, b) in it.product(rev_keys, rev_keys):
    # Divide each pair load by the sum of workload weights for the two reviewers
    rpairs_out.loc[a, b] = rpairs_out.loc[a, b] /(rev_loadwt[a] + rev_loadwt[b])
    # Set the diagonal to zero
    if a == b: rpairs_out.loc[a, b] = 0
# Add a Total row, dividing by (num_revs - 1) to avoid double counting
rpairs_out.loc['Total'] = rpairs_out.sum(axis=0) / (num_revs - 1)
# Find lowest and highest total expertise in selected reviewer assignment table
lo_hi = (min(op_revs[sel_wt]['Expertise']), max(op_revs[sel_wt]['Expertise']) + 1)
# Create a zeroed area x total expertise DataFrame to hold area-expertise loads
area_exp = pd.DataFrame(data=0, index=area_keys, columns=range(lo_hi[0], lo_hi[1]))
# Cycle through outputs and accumulate results for area-expertise count
for i in op_keys:
    area_exp.loc[outputs.loc[i, 'Area #'], outputs.loc[i, 'Expertise']] += 1
# Insert an intial column with the research area description
area_exp.insert(0, 'Area Description', expertise['Area Description'])

# Prepare allocation run summary and output results to spreadsheet
##################################################################
# Create index for run summary Series
sum_index = ['Input File', 'Version', 'Rev Wt', 'RSD', 'PSD', 'Tot Var']
# Create data for run summary Series
sum_data = [filename, 'v7.5', wts[sel_wt], rsd[sel_wt], psd[sel_wt], tvar[sel_wt]]
# Create the indexed Series with run summary
summary = pd.Series(data=sum_data, index=sum_index)
# Append the strategy; squeeze turns single column DataFrame to Series
summary = summary.append(strategy.squeeze())
# Capture date and time for use in output filename
now = dt.now()
# Extract the path of the input file by splitting at rightmost /
part = filename.rpartition('/') 
# Create output xlsx filename by adding 'Allocation Output' and date-time to path
fileout = part[0] + '/Allocation Output ' + now.strftime("%Y-%m-%d(%H%M)") + '.xlsx'
# Output all results to spreadsheet
with pd.ExcelWriter(fileout) as writer:
    outputs.to_excel(writer, sheet_name='Allocation Results')
    rpairs_out.to_excel(writer, sheet_name='Reviewer Pairings')
    inst_rev[sel_wt].to_excel(writer, sheet_name='Reviewer-Institute')
    rev_level[sel_wt].to_excel(writer, sheet_name='Reviewer-Level')
    area_exp.to_excel(writer, sheet_name='Area-Expertise')
    summary.to_excel(writer, sheet_name='Allocation Run Summary')



